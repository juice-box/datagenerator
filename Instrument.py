import numpy


class Instrument():

    def __init__(self, name, variance, drift, startingPrice):
        self.name = name
        self.__variance = variance
        self.__price = startingPrice
        self.__drift = drift
        self.__startingPrice = startingPrice

    def getVarianceTesting(self):
        return self.__variance

    def getDriftTesting(self):
        return self.__drift

    def getStartinPriceTesting(self):
        return self.__startingPrice

    def calculateNextPrice(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0,1)*self.__variance + self.__drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < self.__startingPrice*0.4:
            self.__drift =(-0.7 * self.__drift )
        self.__price = newPrice*1.01 if direction=='B' else newPrice*0.99
        return self.__price