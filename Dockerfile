FROM ubuntu:latest
MAINTAINER Louis Walsh "Baichwal@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python python3-pip 
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3"]
CMD [ "app.py" ]